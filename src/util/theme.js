export default {
  palette: {
    primary: {
      light: "#ffb74d",
      main: "#ff9800",
      dark: "#f57c00",
      contrastText: "#fff",
    },
    secondary: {
      light: "#0066ff",
      main: "#0044ff",
      dark: "#b22a00",
      contrastText: "#ffcc00",
    },
  },
  typography: {
    useNextVariants: true,
  },
};
