const VERSION = "1.3";

importScripts("workbox-v3.6.3/workbox-sw.js");

workbox.setConfig({ modulePathPrefix: "workbox-v3.6.3/" });

const precacheManifest = [];

workbox.precaching.suppressWarnings();
workbox.precaching.precacheAndRoute(precacheManifest);

const dataCacheConfig = {
  cacheName: `it-poslovi-data-v${VERSION}`,
};

workbox.routing.registerRoute(
  /.*cards/,
  workbox.strategies.networkFirst(dataCacheConfig)
);

workbox.routing.registerRoute(
  /.*employer/,
  workbox.strategies.staleWhileRevalidate(dataCacheConfig)
);

workbox.routing.registerRoute(
  /.*employee/,
  workbox.strategies.staleWhileRevalidate(dataCacheConfig)
);

workbox.routing.registerRoute(
  /.*login/,
  workbox.strategies.staleWhileRevalidate(dataCacheConfig)
);
workbox.routing.registerRoute(
  /.*signup/,
  workbox.strategies.staleWhileRevalidate(dataCacheConfig)
);

workbox.routing.registerRoute(
  /.*users*/,
  workbox.strategies.networkFirst({
    cacheName: `users-cache-v${VERSION}`,
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50,
        //mode: 'no-cors' // Try with this
      }),
    ],
  })
);

workbox.routing.registerRoute(
  /.*card*/,
  workbox.strategies.networkFirst({
    cacheName: `card-cache-v${VERSION}`,
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 50,
      }),
    ],
  })
);

workbox.routing.registerRoute(
  /.*profile*/,
  workbox.strategies.networkFirst(dataCacheConfig)
);

workbox.routing.registerRoute(
  /.*.(?:png|jpg|jpeg|svg)$/,
  workbox.strategies.staleWhileRevalidate({
    cacheName: "it-poslovi-images",
  }),
  "GET"
);

// INSTALL
this.self.addEventListener("install", function (event) {
  const channel = new BroadcastChannel("service-worker-channel");
  channel.postMessage({ promptToReload: true });
  channel.onmessage = (message) => {
    if (message.data.skipWaiting) {
      this.self.skipWaiting();
    }
  };
});

// ACTIVATE
self.addEventListener("activate", (e) => {
  console.log("Service Worker: Activated");
  //Remove unwanted caches
  e.waitUntil(
    caches.keys().then((cacheNames) => {
      return Promise.all(
        cacheNames.map((cache) => {
          const cacheName = cache;
          console.log(cacheName);
          let initCache = cache.split("-");
          console.log(initCache);
          initCache = initCache[0];
          console.log(initCache);
          console.log(
            cacheName[cacheName.length - 1] !== VERSION &&
              initCache !== "workbox"
          );

          if (
            cacheName[cacheName.length - 1] !== VERSION &&
            initCache !== "workbox"
          ) {
            console.log("Service Worker: Clearing old cache");
            return caches.delete(cache);
          }
        })
      );
    })
  );
});

// FETCH
self.addEventListener("fetch", (e) => {
  console.log("Service Worker: Fetching");
  e.respondWith(fetch(e.request).catch(() => caches.match(e.request)));
});
