import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import { Link } from "react-router-dom";

//MUI stuff
import Button from "@material-ui/core/Button";
import MuiLink from "@material-ui/core/Link";
import Computer from "@material-ui/icons/Computer";

//Redux stuff
import { connect } from "react-redux";
import { getUsersByTech } from "../redux/actions/dataActions";

class usersByTech extends Component {
  state = {
    techName: "",
  };
  componentDidMount() {}

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleClick = () => {
    this.setState({
      techName: document.getElementById("techNameId").value,
    });
    this.props.getUsersByTech(this.state.techName.trim());
  };

  render() {
    const { usersbytech, authenticated } = this.props;
    let userbytechMarkup;
    if (usersbytech) {
      userbytechMarkup = usersbytech.map((user) => (
        <div key={user.username} style={{ margin: 20 }}>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <MuiLink
              component={Link}
              to={`/users/${user.username}`}
              color="primary"
              //variant="h6"
              style={{
                width: "30%",
              }}
            >
              <p>{user.employerName}</p>
            </MuiLink>

            {user.role === "employee" ? (
              <p
                style={{
                  width: "20%",
                  color: "#d4d3cf",
                }}
              >
                (Posloprimac)
              </p>
            ) : (
              <p style={{ color: "#d4d3cf" }}>(Poslodavac)</p>
            )}
          </div>

          <Fragment>
            <Computer color="primary" style={{ verticalAlign: "middle" }} />
            {user.other}
          </Fragment>
          <hr />
        </div>
      ));
    }

    const search = authenticated ? (
      <div>
        <div style={{ textAlign: "center" }}>
          <TextField
            id="techNameId"
            name="techName"
            type="text"
            label="Pretraži po tehnologijama"
            rows="1"
            value={this.state.techName}
            placeholder="Upiši traženu tehnologiju"
            onChange={this.handleChange}
            fullWidth
            variant="outlined"
            style={{ margin: 50, marginBottom: 10, width: "80%" }}
          ></TextField>
          <hr style={{ border: "none" }} />
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleClick}
          >
            Traži
          </Button>
        </div>
        {userbytechMarkup}
      </div>
    ) : (
      <h1>Molimo prijavite se za nastavak</h1>
    );
    return <div>{search}</div>;
  }
}

usersByTech.propTypes = {
  getUsersByTech: PropTypes.func.isRequired,
};
const mapStateToProps = (state) => ({
  usersbytech: state.data.usersbytech,
  authenticated: state.user.authenticated,
});
export default connect(mapStateToProps, { getUsersByTech })(usersByTech);
