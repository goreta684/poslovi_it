import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

// MUI stuff
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import { LinearProgress } from "@material-ui/core";

// Redux stuff
import { connect } from "react-redux";
import { signupUser } from "../redux/actions/userActions";

const styles = {
  form: {
    textAlign: "center",
  },
  pageTitle: {
    margin: "20px auto 20px auto",
  },
  textField: {
    margin: "10px auto 10px auto",
    width: "100%",
  },
  button: {
    margin: "20px auto 20px auto",
    position: "relative",
  },
  middleContainer: {
    margin: "10px",
  },
  customError: {
    color: "red",
    fontSize: "0.8rem",
    marginTop: 10,
  },
  progress: {
    position: "absolute",
  },
};

class signup extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      confirmPassword: "",
      username: "",
      employerName: "",
      role: "employee",
      errors: {},
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.UI.errors) {
      this.setState({ errors: nextProps.UI.errors });
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({
      loading: true,
    });
    const newUserData = {
      email: this.state.email,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
      username: this.state.username,
      employerName: this.state.employerName,
      role: this.state.role,
    };
    this.props.signupUser(newUserData, this.props.history);
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleChangeSelect = (event) => {
    this.setState({ role: event.target.value });
    event.preventDefault();
  };
  render() {
    const {
      classes,
      UI: { loading },
    } = this.props;
    const { errors } = this.state;

    return (
      <Grid container className={classes.form}>
        <Grid item sm />
        <Grid item sm className={classes.middleContainer}>
          <Typography variant="h2" className={classes.pageTitle}>
            Registracija
          </Typography>
          <form noValidate onSubmit={this.handleSubmit}>
            <TextField
              id="email"
              name="email"
              type="email"
              label="Email adresa"
              className={classes.textField}
              helperText={errors.email}
              error={errors.email ? true : false}
              value={this.state.email}
              onChange={this.handleChange}
              variant="outlined"
              fullWidth
            />
            <TextField
              id="password"
              name="password"
              type="password"
              label="Lozinka"
              className={classes.textField}
              helperText={errors.password}
              error={errors.password ? true : false}
              value={this.state.password}
              onChange={this.handleChange}
              variant="outlined"
              fullWidth
            />
            <TextField
              id="confirmPassword"
              name="confirmPassword"
              type="password"
              label="Ponovite lozinku"
              className={classes.textField}
              helperText={errors.confirmPassword}
              error={errors.confirmPassword ? true : false}
              value={this.state.confirmPassword}
              onChange={this.handleChange}
              variant="outlined"
              fullWidth
            />
            <TextField
              id="username"
              name="username"
              type="text"
              label="Korisničko ime"
              className={classes.textField}
              helperText={errors.username}
              error={errors.username ? true : false}
              value={this.state.username}
              onChange={this.handleChange}
              variant="outlined"
              fullWidth
            />
            <TextField
              id="employerName"
              name="employerName"
              type="text"
              label="Ime tvrtke / Vaše ime"
              className={classes.textField}
              helperText={errors.employerName}
              error={errors.employerName ? true : false}
              value={this.state.employerName}
              onChange={this.handleChange}
              variant="outlined"
              fullWidth
            />

            {errors.general && (
              <Typography variant="body2" className={classes.customError}>
                {errors.general}
              </Typography>
            )}
            <div
              style={{
                width: "100%",
              }}
            >
              <select
                value={this.state.value}
                onChange={this.handleChangeSelect}
                style={{
                  margin: "10px auto 10px auto",
                  width: "100%",
                  height: 60,
                  backgroundColor: "#f2f2f2",
                  color: "#919090",
                  fontSize: 16,
                  padding: 10,
                }}
              >
                <option value="employee">Tražim posao</option>
                <option value="employer">Nudim posao</option>
              </select>
            </div>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              className={classes.button}
              disabled={loading}
            >
              Registracija
            </Button>
            {loading && <LinearProgress />}

            <br />
            <small>
              Već imate korisnički račun? Prijavite se{" "}
              <Link to="/login">ovdje</Link>
            </small>
          </form>
        </Grid>
        <Grid item sm />
      </Grid>
    );
  }
}

signup.propTypes = {
  classes: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  UI: PropTypes.object.isRequired,
  signupUser: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
  UI: state.UI,
});

export default connect(mapStateToProps, { signupUser })(
  withStyles(styles)(signup)
);
