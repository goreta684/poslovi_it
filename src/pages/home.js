import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Card from "../components/card/Card";
import PropTypes from "prop-types";
import CircularProgress from "@material-ui/core/CircularProgress";
import Pagination from "@material-ui/lab/Pagination";

// Redux stuff
import { connect } from "react-redux";
import { getCards } from "../redux/actions/dataActions";

class home extends Component {
  state = {
    currentPage: 1,
    cardsPerPage: 3,
  };
  componentDidMount() {
    this.props.getCards();
  }

  handleChangeSelect = (event) => {
    this.setState({ cardsPerPage: event.target.value });
    event.preventDefault();
  };

  render() {
    // Get current cards
    const { cardsPerPage, currentPage } = this.state;
    const indexOfLastCard = currentPage * cardsPerPage;
    const indexOfFirstCard = indexOfLastCard - cardsPerPage;

    const handleChange = (event, value) => {
      this.setState({
        currentPage: value,
      });
      window.scrollTo(0, 0);
    };

    const { cards, loading } = this.props.data;
    let currentCards;
    let recentCardsMarkup;
    if (cards) {
      currentCards = cards.slice(indexOfFirstCard, indexOfLastCard);

      recentCardsMarkup = !loading ? (
        cards.length === 0 ? (
          <h1 style={{ color: "orange", textAlign: "center" }}>
            Trenutno nema dostupnih oglasa!
          </h1>
        ) : (
          currentCards.map((card) => <Card key={card.cardId} card={card} />)
        )
      ) : (
        <Grid
          container
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Grid item sm />
          <Grid item sm>
            <CircularProgress
              size={200}
              style={{
                marginTop: "30%",
              }}
            />
          </Grid>
          <Grid item sm />
        </Grid>
      );

      return (
        <div>
          {!loading ? (
            <div
              style={{
                marginLeft: "6%",
                marginRight: "6%",
                width: "42%",
                paddingTop: 20,
              }}
            >
              <form>
                <label
                  style={{
                    marginRight: 8,
                    fontSize: 10,
                    color: "#8a8888",
                  }}
                >
                  Oglasa po stranici:
                </label>
                <select
                  value={this.state.value}
                  onChange={this.handleChangeSelect}
                >
                  <option value="3">3</option>
                  <option value="5">5</option>
                  <option value="10">10</option>
                  <option value="20">20</option>
                </select>
                <label
                  style={{
                    marginleft: 10,
                    fontSize: 10,
                    color: "#8a8888",
                    paddingLeft: 8,
                  }}
                >
                  od {cards.length}
                </label>
              </form>
              {/* <Search />*/}
            </div>
          ) : null}

          <Grid container>
            <Grid item sm />
            <Grid
              item
              xs={12}
              lg={10}
              style={{
                marginTop: 30,
                marginLeft: 10,
                marginRight: 10,
                marginBottom: 20,
              }}
            >
              {recentCardsMarkup}
            </Grid>
            <Grid item sm />
          </Grid>
          <div
            style={{
              marginLeft: "10%",
              marginRight: "10%",
            }}
          >
            {!loading ? (
              <Pagination
                count={Math.ceil(cards.length / cardsPerPage)}
                color="primary"
                onChange={handleChange}
                style={{
                  width: "100%",
                  marginBottom: 20,
                }}
              />
            ) : null}
          </div>
        </div>
      );
    } else {
      return <p>Problemi sa konekcijom</p>;
    }
  }
}

home.propTypes = {
  getCards: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  data: state.data,
  user: state.user,
});

export default connect(mapStateToProps, {
  getCards,
})(home);
