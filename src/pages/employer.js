import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Employer from "../components/users/Employer";
import PropTypes from "prop-types";
import Search from "../components/layout/SearchEmployers";

// MUI stuff
import CircularProgress from "@material-ui/core/CircularProgress";

// Redux stuff
import { connect } from "react-redux";
import { getAllEmployers } from "../redux/actions/dataActions";

class employer extends Component {
  componentDidMount() {
    this.props.getAllEmployers();
  }
  render() {
    const { employers, loading } = this.props.data;

    if (employers) {
      let employersList =
        !employers || !loading ? (
          employers.length === 0 ? (
            <Grid
              container
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Grid item sm />
              <Grid item sm>
                <CircularProgress size={200} style={{ marginTop: "30%" }} />
              </Grid>
              <Grid item sm />
            </Grid>
          ) : (
            employers.map((employer, i) => (
              <Employer key={employer.userId} employer={employer} />
            ))
          )
        ) : (
          <Grid
            container
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Grid item sm />
            <Grid item sm>
              <CircularProgress size={200} style={{ marginTop: "30%" }} />
            </Grid>
            <Grid item sm />
          </Grid>
        );
      return (
        <Grid container>
          <Grid item sm />
          <Grid
            item
            xs={12}
            lg={10}
            style={{
              marginTop: 30,
              marginLeft: 10,
              marginRight: 10,
              marginBottom: 20,
            }}
          >
            <div
              style={{
                width: "98%",
              }}
            >
              <Search />
            </div>

            <h1
              style={{
                fontStyle: "normal",
                fontWeight: 100,
                fontVariant: "small-caps",
                fontFamily: "Arial, serif",
              }}
            >
              Poslodavci
            </h1>
            {employersList}
          </Grid>
          <Grid item sm />
        </Grid>
      );
    } else {
      return (
        <Grid
          container
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Grid item sm />
          <Grid item sm>
            <CircularProgress size={200} style={{ marginTop: "30%" }} />
            <h1 style={{ color: "orange" }}>
              Traženi podaci su trenutno nedostupni!
            </h1>
          </Grid>
          <Grid item sm />
        </Grid>
      );
    }
  }
}

employer.propTypes = {
  getAllEmployers: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  data: state.data,
});
export default connect(mapStateToProps, {
  getAllEmployers,
})(employer);
