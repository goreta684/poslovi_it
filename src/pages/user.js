import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import Card from "../components/card/Card";
import StaticProfile from "../components/profile/StaticProfile";
import { Link } from "react-router-dom";

// MUI stuff
import Grid from "@material-ui/core/Grid";
import CircularProgress from "@material-ui/core/CircularProgress";
import MuiLink from "@material-ui/core/Link";

// Redux stuff
import { connect } from "react-redux";
import { getUserData } from "../redux/actions/dataActions";

export class user extends Component {
  state = {
    profile: null,
    cardIdParam: null,
  };

  componentDidMount() {
    // match - holds details about url, path, base  url and other stuff
    // params - parameters from App.js
    // username - we have acces to only one parameter which is username from App.js route
    const username = this.props.match.params.username;
    const cardId = this.props.match.params.cardId;

    if (cardId) this.setState({ cardIdParam: cardId });

    this.props.getUserData(username);
    axios
      .get(`/user/${username}`)
      .then((res) => {
        this.setState({
          profile: res.data.user,
        });
      })
      .catch((err) => console.log(err));
  }
  render() {
    const { cards, loading } = this.props.data;
    const { cardIdParam } = this.state;
    const username = this.props.match.params.username;
    let cardsMarkup;
    if (cards) {
      cardsMarkup =
        loading || cards.length === null ? ( //added cards.length === null
          <Grid
            container
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Grid item sm />
            <Grid item sm>
              <CircularProgress size={200} style={{ marginTop: "30%" }} />
            </Grid>
            <Grid item sm />
          </Grid>
        ) : cards.length === 0 ? (
          <h1 style={{ color: "orange", textAlign: "center" }}>
            Trenutno nema oglasa od korisnika{" "}
            <MuiLink
              component={Link}
              to={`/users/${username}`}
              color="primary"
              variant="h4"
            >
              @{username}
            </MuiLink>
          </h1>
        ) : !cardIdParam ? (
          cards.map((card) => <Card key={card.cardId} card={card} />)
        ) : (
          cards.map((card) => {
            if (card.cardId !== cardIdParam)
              return <Card key={card.cardId} card={card} />;
            else return <Card key={card.cardId} card={card} openDialog />;
          })
        );
    }

    return (
      <Grid
        container
        spacing={2}
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Grid
          item
          xs={12}
          style={{
            flex: 1,
            width: "80%",
            margin: 20,
          }}
        >
          {this.state.profile === null ? (
            <Grid
              container
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Grid item sm />
              <Grid item sm>
                <CircularProgress size={200} style={{ marginTop: "30%" }} />
              </Grid>
              <Grid item sm />
            </Grid>
          ) : (
            <StaticProfile profile={this.state.profile} />
          )}
        </Grid>
        <Grid container>
          <Grid item sm />
          <Grid
            item
            xs={12}
            lg={10}
            style={{
              marginTop: 30,
              marginLeft: 20,
              marginRight: 20,
              marginBottom: 20,
            }}
          >
            {cardsMarkup}
          </Grid>
          <Grid item sm />
        </Grid>
      </Grid>
    );
  }
}

user.propTypes = {
  getUserData: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  data: state.data,
});

export default connect(mapStateToProps, { getUserData })(user);
