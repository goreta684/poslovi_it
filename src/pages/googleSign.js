// Import FirebaseAuth and firebase.
import React from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import firebase from "firebase";

/*const userData = {
    email: this.state.email,
    password: this.state.password,
  };
  this.props.loginUser(userData, this.props.history);
*/
// Configure Firebase.
const config = {
  apiKey: "AIzaSyAmExCbO-Z9GxJbMpDeTBLqVSlmhrlX13U",
  authDomain: "poslovi-it.firebaseapp.com",
};
firebase.initializeApp(config);

class googleSign extends React.Component {
  // The component's Local state.
  state = {
    isSignedIn: false, // Local signed-in state.
  };

  uiConfig = {
    signInFlow: "popup",
    signInOptions: [
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.GithubAuthProvider.PROVIDER_ID,
    ],
    callbacks: {
      // Avoid redirects after sign-in.
      signInSuccessWithAuthResult: () => false,
    },
  };

  // Listen to the Firebase Auth state and set the local state.
  componentDidMount() {
    this.unregisterAuthObserver = firebase
      .auth()
      .onAuthStateChanged((user) => this.setState({ isSignedIn: !!user }));
  }

  // Make sure we un-register Firebase observers when the component unmounts.
  componentWillUnmount() {
    this.unregisterAuthObserver();
  }

  render() {
    if (!this.state.isSignedIn) {
      return (
        <div>
          <h1>My App</h1>
          <p>Please sign-in:</p>
          <StyledFirebaseAuth
            uiConfig={this.uiConfig}
            firebaseAuth={firebase.auth()}
          />
        </div>
      );
    }
    return (
      <div>
        {console.log(firebase.auth().currentUser.email)}
        {console.log(firebase.auth().currentUser.displayName)}
        {console.log(firebase.auth().currentUser.providerId)}
        <h1>My App</h1>
        <p>
          Welcome {firebase.auth().currentUser.displayName}! You are now
          signed-in!
        </p>
        <p>{firebase.auth().currentUser.email}</p>
        <p>{firebase.auth().currentUser.providerId}</p>
        <a href onClick={() => firebase.auth().signOut()}>
          Sign-out
        </a>
      </div>
    );
  }
}
//data.user.getIdToken();
export default googleSign;
