import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Employee from "../components/users/Employee";
import PropTypes from "prop-types";
import Search from "../components/layout/SearchEmployees";

// MUI Stuff
import CircularProgress from "@material-ui/core/CircularProgress";

// Redux Stuff
import { connect } from "react-redux";
import { getAllEmployees } from "../redux/actions/dataActions";

class employee extends Component {
  componentDidMount() {
    this.props.getAllEmployees();
  }

  render() {
    const { employees, loading } = this.props.data;

    if (employees) {
      let employeesList =
        !employees || !loading ? (
          employees.length === 0 ? (
            <Grid
              container
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Grid item sm />
              <Grid item sm>
                <CircularProgress size={200} style={{ marginTop: "30%" }} />
              </Grid>
              <Grid item sm />
            </Grid>
          ) : (
            employees.map((employee, i) => (
              <Employee key={employee.userId} employee={employee} />
            ))
          )
        ) : (
          <Grid
            container
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Grid item sm />
            <Grid item sm>
              <CircularProgress size={200} style={{ marginTop: "30%" }} />
            </Grid>
            <Grid item sm />
          </Grid>
        );
      return (
        <Grid container>
          <Grid item sm />
          <Grid
            item
            xs={12}
            lg={10}
            style={{
              marginTop: 30,
              marginLeft: 10,
              marginRight: 10,
              marginBottom: 20,
            }}
          >
            <Search />
            <h1
              style={{
                fontStyle: "normal",
                fontWeight: 100,
                fontVariant: "small-caps",
                fontFamily: "Arial, serif",
              }}
            >
              Posloprimci
            </h1>
            {employeesList}
          </Grid>
          <Grid item sm />
        </Grid>
      );
    } else {
      return (
        <Grid
          container
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Grid item sm />
          <Grid item sm>
            <CircularProgress size={200} style={{ marginTop: "30%" }} />
            <h1 style={{ color: "orange" }}>
              Traženi podaci su trenutno nedostupni!
            </h1>
          </Grid>
          <Grid item sm />
        </Grid>
      );
    }
  }
}

employee.propTypes = {
  getAllEmployees: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  data: state.data,
});

export default connect(mapStateToProps, {
  getAllEmployees,
})(employee);
