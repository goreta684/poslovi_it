import {
  SET_CARDS,
  SET_CARD,
  LOADING_DATA,
  LIKE_CARD,
  UNLIKE_CARD,
  DELETE_CARD,
  LOADING_UI,
  POST_CARD,
  SET_ERRORS,
  CLEAR_ERRORS,
  STOP_LOADING_UI,
  SUBMIT_COMMENT,
  EDIT_CARD,
  EDIT_COMMENT,
  DELETE_COMMENT,
  SET_USER_EMPLOYERS,
  SET_USER_EMPLOYEES,
  LOADING_USER,
  SET_ALL_USERS,
  SET_USERS_BY_TECH,
} from "../types";
import axios from "axios";
import fetch from "node-fetch";

const bearerToken = localStorage.getItem("FBIdToken");

const sendRequest = async (url, params = undefined) => {
  return await fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Authorization: bearerToken,
      "Content-Type": "application/json",
    },
    credentials: "include",
    withCredentials: true,
  });
};

const deleteRequest = async (url, params = undefined) => {
  return await fetch(url, {
    method: "DELETE",
    mode: "cors",
    headers: {
      Authorization: bearerToken,
      "Content-Type": "application/json",
    },
    credentials: "include",
    withCredentials: true,
  });
};

// Get All Cards
export const getCards = () => (dispatch) => {
  dispatch({ type: LOADING_DATA });
  sendRequest("/cards")
    .then((res) => res.json())
    .then((json) => {
      dispatch({
        type: SET_CARDS,
        payload: json,
      });
    })
    .catch((err) => {
      dispatch({
        type: SET_CARDS,
        payload: [],
      });
    });
};

// Get Card
export const getCard = (cardId) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  sendRequest(`/card/${cardId}`)
    .then((res) => res.json())
    .then((json) => {
      dispatch({ type: SET_CARD, payload: json });
      dispatch({ type: STOP_LOADING_UI });
    })
    .catch((err) => console.log(err));
};

// Post Card
export const postCard = (newCard) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  axios
    .post("/card", newCard)
    .then((res) => {
      dispatch({
        type: POST_CARD,
        payload: res.data,
      });
      dispatch(clearErrors());
    })
    .catch((err) => {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data,
      });
    });
};

// Like Card
export const likeCard = (cardId) => (dispatch) => {
  axios
    .get(`/card/${cardId}/like`)
    .then((res) => {
      dispatch({
        type: LIKE_CARD,
        payload: res.data,
      });
    })
    .catch((err) => console.log(err));
};

// Unlike Card
export const unlikeCard = (cardId) => (dispatch) => {
  axios
    .get(`/card/${cardId}/unlike`)
    .then((res) => {
      dispatch({
        type: UNLIKE_CARD,
        payload: res.data,
      });
    })
    .catch((err) => console.log(err));
};

//Submit a Comment
export const submitComment = (cardId, commentData) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  dispatch({ type: LOADING_DATA });
  axios
    .post(`/card/${cardId}/comment`, commentData)
    .then((res) => {
      dispatch({
        type: SUBMIT_COMMENT,
        payload: res.data,
      });
      dispatch(clearErrors());
      dispatch({ type: STOP_LOADING_UI });
    })
    .catch((err) => {
      dispatch({ type: SET_ERRORS, payload: err.response.data });
    });
};

// Delete Card
export const deleteCard = (cardId) => (dispatch) => {
  axios
    .delete(`/card/${cardId}`)
    .then(() => {
      dispatch({
        type: DELETE_CARD,
        payload: cardId,
      });
    })
    .catch((err) => console.log(err));
};

// Delete Comment
export const deleteComment = (cardId, commentId) => (dispatch) => {
  let delObj = {};
  deleteRequest(`/cm/${cardId}/${commentId}`)
    .then((res) => res.json())
    .then((json) => {
      delObj.commentId = commentId;
      delObj.commentCount = json;
      delObj.cardId = cardId;
      dispatch({
        type: DELETE_COMMENT,
        payload: delObj,
      });
    })
    .catch((err) => console.log(err));
  /*axios
    .delete(`/cm/${cardId}/${commentId}`)
    .then((res) => {
      console.log(res.data);
      delObj.commentId = commentId;
      delObj.commentCount = res.data;
      delObj.cardId = cardId;
      dispatch({
        type: DELETE_COMMENT,
        payload: delObj,
      });
    })
    .catch((err) => console.log(err));*/
};

// Get user data for Static profile
export const getUserData = (usernameHandle) => (dispatch) => {
  dispatch({ type: LOADING_DATA });
  axios
    .get(`/user/${usernameHandle}`)
    .then((res) => {
      dispatch({
        type: SET_CARDS,
        payload: res.data.cards,
      });
    })
    .catch(() => {
      dispatch({
        type: SET_CARDS,
        payload: null,
      });
    });
};

// Edit Post
export const editPost = (postDetails, cardId) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  axios
    .put(`/card/${cardId}`, postDetails)
    .then((res) => {
      dispatch({
        type: EDIT_CARD,
        payload: res.data,
      });
      dispatch(clearErrors());
      dispatch(getCards());
    })
    .catch((err) => {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data,
      });
    });
};

// Edit Comment
export const editComment = (commDetails, commentId) => (dispatch) => {
  dispatch({ type: LOADING_DATA });
  axios
    .put(`/cm/${commentId}`, commDetails)
    .then((res) => {
      dispatch({
        type: EDIT_COMMENT,
        payload: res.data,
      });
      dispatch(clearErrors());
      dispatch(getCards()); //added 22.5.
    })
    .catch((err) => {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data,
      });
    });
};

// Get All Employers
export const getAllEmployers = () => (dispatch) => {
  dispatch({ type: LOADING_USER });
  axios
    .get("/users/employer")
    .then((res) => {
      dispatch({
        type: SET_USER_EMPLOYERS,
        payload: res.data,
      });
    })
    .catch(() => {
      dispatch({
        type: SET_USER_EMPLOYERS,
        payload: [],
      });
    });
};

// Get All Employees
export const getAllEmployees = () => (dispatch) => {
  dispatch({ type: LOADING_USER });
  sendRequest("/users/employee")
    .then((res) => res.json())
    .then((json) => {
      dispatch({
        type: SET_USER_EMPLOYEES,
        payload: json,
      });
      dispatch(getUserData());
    })
    .catch(() => {
      dispatch({
        type: SET_USER_EMPLOYEES,
        payload: [],
      });
    });
  //refreshanjem posloprimaca stranice javlja error:
  //xhr.js:178 GET https://europe-west1-poslovi-it.cloudfunctions.net/api/user/undefined 404
  /*axios
    .get("/users/employee")
    .then((res) => {
      dispatch({
        type: SET_USER_EMPLOYEES,
        payload: res.data,
      });
    })
    .catch(() => {
      dispatch({
        type: SET_USER_EMPLOYEES,
        payload: [],
      });
    });*/
};

// Set All Users
export const getAllUsers = () => (dispatch) => {
  dispatch({ type: LOADING_USER });
  axios
    .get("/users")
    .then((res) => {
      dispatch({
        type: SET_ALL_USERS,
        payload: res.data,
      });
    })
    .catch(() => {
      dispatch({
        type: SET_ALL_USERS,
        payload: [],
      });
    });
};

export const getUsersByTech = (techName) => (dispatch) => {
  dispatch({ type: LOADING_USER });
  axios
    .get(`/tech/${techName}`)
    .then((res) => {
      dispatch({
        type: SET_USERS_BY_TECH,
        payload: res.data,
      });
    })
    .catch(() => {
      dispatch({
        type: SET_USERS_BY_TECH,
        payload: [],
      });
    });
};

// Clear Errors
export const clearErrors = () => (dispatch) => {
  dispatch({ type: CLEAR_ERRORS });
};
