import {
  SET_USER,
  SET_ERRORS,
  CLEAR_ERRORS,
  LOADING_UI,
  SET_UNAUTHENTICATED,
  LOADING_USER,
  MARK_NOTIFICATIONS_READ,
  FOLLOW_USER,
  UNFOLLOW_USER,
} from "../types";
import axios from "axios";
import fetch from "node-fetch";

const bearerToken = localStorage.getItem("FBIdToken");

// Login User
export const loginUser = (userData, history) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  axios
    .post("/login", userData)
    .then((res) => {
      setAuthorizationHeader(res.data.token);
      dispatch(getUserData());
      dispatch({ type: CLEAR_ERRORS });
      history.push("/");
    })
    .catch((err) => {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data,
      });
    });
};

// Logout User
export const logoutUser = () => (dispatch) => {
  localStorage.removeItem("FBIdToken");
  delete axios.defaults.headers.common["Authorization"];
  dispatch({ type: SET_UNAUTHENTICATED });
};

// Signup User
export const signupUser = (newUserData, history) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  axios
    .post("/signup", newUserData)
    .then((res) => {
      setAuthorizationHeader(res.data.token);
      dispatch(getUserData());
      dispatch({ type: CLEAR_ERRORS });
      history.push("/");
    })
    .catch((err) => {
      dispatch({
        type: SET_ERRORS,
        payload: err.response.data,
      });
    });
};

// Get User Data
export const getUserData = () => (dispatch) => {
  dispatch({ type: LOADING_USER });
  axios
    .get("/user")
    .then((res) => {
      dispatch({
        type: SET_USER,
        payload: res.data,
      });
    })
    .catch((err) => console.log(err));
};

// Upload Image
export const uploadImage = (formData) => (dispatch) => {
  dispatch({ type: LOADING_USER });
  axios
    .post("/user/image", formData)
    .then(() => {
      dispatch(getUserData());
    })
    .catch((err) => console.log(err));
};

// Edit User Details
export const editUserDetails = (userDetails) => (dispatch) => {
  dispatch({ type: LOADING_USER });

  axios
    .post("/user", userDetails)
    .then(() => {
      dispatch(getUserData());
    })
    .catch((err) => {
      console.log(err);
    });
};

// Mark Notifications Read
export const markNotificationsRead = (notificationIds) => (dispatch) => {
  axios
    .post("/notifications", notificationIds)
    .then(() => {
      dispatch({
        type: MARK_NOTIFICATIONS_READ,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

const sendRequest = async (url, params = undefined) => {
  return await fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Authorization: bearerToken,
      "Content-Type": "application/json",
    },
    credentials: "include",
    withCredentials: true,
  });
};

// Follow User
export const followUser = (username) => (dispatch) => {
  sendRequest(`/user/${username}/follow`)
    .then((res) => res.json())
    .then((json) => {
      dispatch({
        type: FOLLOW_USER,
        payload: json,
      });
      dispatch(getUserData());
    })
    .catch((err) => console.log(err));
};

// UnFollow User
export const unFollowUser = (username) => (dispatch) => {
  sendRequest(`/user/${username}/unfollow`)
    .then((res) => res.json())
    .then((json) => {
      dispatch({
        type: UNFOLLOW_USER,
        payload: json,
      });
      dispatch(getUserData());
    })
    .catch((err) => console.log(err));
};

// Set Authorization Header
const setAuthorizationHeader = (token) => {
  const FBIdToken = `Bearer ${token}`;
  localStorage.setItem("FBIdToken", FBIdToken);
  axios.defaults.headers.common["Authorization"] = FBIdToken;
};
