// User reducer types
export const SET_AUTHENTICATED = "SET_AUTHENTICATED";
export const SET_UNAUTHENTICATED = "SET_UNAUTHENTICATED";
export const SET_USER = "SET_USER";
export const SET_USER_EMPLOYERS = "SET_USER_EMPLOYERS";
export const SET_USER_EMPLOYEES = "SET_USER_EMPLOYEES";
export const SET_ALL_USERS = "SET_ALL_USERS"; //?
export const LOADING_USER = "LOADING_USER";
export const MARK_NOTIFICATIONS_READ = "MARK_NOTIFICATIONS_READ";
export const FOLLOW_USER = "FOLLOW_USER";
export const UNFOLLOW_USER = "UNFOLLOW_USER";
export const SET_USERS_BY_TECH = "SET_USERS_BY_TECH";

// UI reducer types
export const SET_ERRORS = "SET_ERRORS";
export const LOADING_UI = "LOADING_UI";
export const CLEAR_ERRORS = "CLEAR_ERRORS";
export const LOADING_DATA = "LOADING_DATA";
export const STOP_LOADING_UI = "STOP_LOADING_UI";

//Data reducer types
export const SET_CARDS = "SET_CARDS";
export const SET_CARD = "SET_CARD";
export const LIKE_CARD = "LIKE_CARD";
export const UNLIKE_CARD = "UNLIKE_CARD";
export const DELETE_CARD = "DELETE_CARD";
export const POST_CARD = "POST_CARD";
export const SUBMIT_COMMENT = "SUBMIT_COMMENT";
export const DELETE_COMMENT = "DELETE_COMMENT";
export const EDIT_CARD = "EDIT_CARD";
export const EDIT_COMMENT = "EDIT_COMMENT";
