import {
  SET_CARDS,
  SET_CARD,
  LIKE_CARD,
  UNLIKE_CARD,
  LOADING_DATA,
  DELETE_CARD,
  POST_CARD,
  SUBMIT_COMMENT,
  EDIT_CARD,
  EDIT_COMMENT,
  DELETE_COMMENT,
  SET_USER_EMPLOYERS,
  SET_USER_EMPLOYEES,
  SET_ALL_USERS,
  SET_USERS_BY_TECH,
} from "../types";

const initialState = {
  cards: [],
  card: {},
  loading: false,
  comments: [],
  employers: [],
  employees: [],
  allUsers: [],
  usersbytech: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOADING_DATA:
      return {
        ...state,
        loading: true,
      };
    case SET_CARDS:
      return {
        ...state,
        cards: action.payload,
        loading: false,
      };
    case SET_CARD:
      return {
        ...state,
        card: action.payload,
        comments: action.payload.comments,
      };
    case LIKE_CARD:
    case UNLIKE_CARD:
      let index = state.cards.findIndex(
        (card) => card.cardId === action.payload.cardId
      );
      state.cards[index] = action.payload;
      if (state.card.cardId === action.payload.cardId) {
        state.card = action.payload;
      }
      return {
        ...state,
      };
    case DELETE_CARD:
      let i = state.cards.findIndex((card) => card.cardId === action.payload);
      state.cards.splice(i, 1);
      return {
        ...state,
      };
    case DELETE_COMMENT:
      return {
        ...state,
        comments: state.comments.filter(
          (comm) => comm.commentId !== action.payload.commentId
        ),
        card: {
          ...state.card,
          comments: state.card.comments.filter(
            (comm) => comm.commentId !== action.payload.commentId
          ),
          commentCount: action.payload.commentCount,
        },
      };
    case POST_CARD:
      return {
        ...state,
        cards: [
          action.payload, //new card will be first in the array because is newest
          ...state.cards, // other card after new
        ],
      };
    case EDIT_CARD:
      return {
        ...state,
        card: action.payload,
        loading: false, //?
      };
    case EDIT_COMMENT:
      return {
        ...state,
        comments: action.payload,
        loading: false, //?
      };
    case SUBMIT_COMMENT:
      let s_c_index = state.cards.findIndex(
        (card) => card.cardId === action.payload.cardId
      );
      state.cards[s_c_index].commentCount = action.payload.commCount;
      return {
        ...state,
        card: {
          ...state.card,
          comments: [action.payload, ...state.card.comments],
          commentCount: action.payload.commCount,
        },
        comments: [action.payload, ...state.comments],

        loading: false,
      };
    case SET_USER_EMPLOYERS:
      return {
        ...state,
        employers: action.payload,
        loading: false,
      };
    case SET_USER_EMPLOYEES:
      return {
        ...state,
        employees: action.payload,
        loading: false,
      };
    case SET_ALL_USERS:
      return {
        ...state,
        allUsers: action.payload,
        loading: false,
      };
    case SET_USERS_BY_TECH:
      return {
        ...state,
        usersbytech: action.payload,
        loading: false,
      };
    default:
      return state;
  }
}
