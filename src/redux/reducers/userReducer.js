import {
  SET_USER,
  SET_AUTHENTICATED,
  SET_UNAUTHENTICATED,
  LOADING_USER,
  LIKE_CARD,
  UNLIKE_CARD,
  MARK_NOTIFICATIONS_READ,
  FOLLOW_USER,
  UNFOLLOW_USER,
} from "../types";

const initialState = {
  authenticated: false,
  loading: false,
  credentials: {},
  likes: [],
  notifications: [],
  followers: [],
  following: [],
  follow: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_AUTHENTICATED:
      return {
        ...state,
        authenticated: true,
      };
    case SET_UNAUTHENTICATED:
      return initialState;
    case SET_USER:
      return {
        ...state, //added
        authenticated: true,
        loading: false,
        ...action.payload, //bind stuff from initial state, cred to cred, likes to likes etc
      };
    case LOADING_USER:
      return {
        ...state,
        loading: true,
      };
    case LIKE_CARD:
      return {
        ...state,
        likes: [
          ...state.likes,
          {
            usernameHandle: state.credentials.username,
            cardId: action.payload.cardId,
          },
        ],
      };
    case UNLIKE_CARD:
      return {
        ...state,
        likes: state.likes.filter(
          (like) => like.cardId !== action.payload.cardId
        ),
      };
    case FOLLOW_USER:
      return {
        ...state,
        follow: [
          ...state.follow,
          {
            sender: action.payload.senderData.username,
            recipient: action.payload.userData.username,
          },
        ],
      };
    case UNFOLLOW_USER:
      state.credentials.following = action.payload.senderData.following;
      return {
        ...state,
        following: state.following.filter(
          (f) => f.recipient !== action.payload.userData.username
        ),
      };
    case MARK_NOTIFICATIONS_READ:
      state.notifications.forEach((not) => (not.read = true));
      return {
        ...state,
      };
    default:
      return state;
  }
}
