import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import MyButton from "../../util/MyButton";
import PostCard from "../card/PostCard";
import Notifications from "./Notifications";
import Menu from "./Menu";

// MUI stuff
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";

//Icons
import HomeIcon from "@material-ui/icons/Home";
import Logo from "../../images/icon/icon-logo.png";
import VpnKey from "@material-ui/icons/VpnKey";
import Search from "@material-ui/icons/Search";

const Link = require("react-router-dom").Link;

class Navbar extends Component {
  render() {
    const { authenticated } = this.props;
    return (
      <AppBar>
        <Toolbar className="nav-container">
          {authenticated ? (
            <div className="nav-fragment">
              <div className="nav-fragment1">
                <Link to="/">
                  <img
                    src={Logo}
                    alt="Logo"
                    style={{ width: 50, height: 50 }}
                  />
                </Link>
              </div>
              <div className="nav-fragment2">
                <PostCard />
                <Link to="/tech">
                  <MyButton tip="Pretraži">
                    <Search />
                  </MyButton>
                </Link>
                <Link to="/">
                  <MyButton tip="Naslovna">
                    <HomeIcon />
                  </MyButton>
                </Link>
                <Notifications />
                <Menu />
              </div>
            </div>
          ) : (
            <div className="nav-fragment">
              <div className="nav-fragment1">
                <Link to="/">
                  <img
                    src={Logo}
                    alt="Logo"
                    style={{ width: 50, height: 50 }}
                  />
                </Link>
              </div>
              <div className="nav-fragment2">
                <Button color="inherit" component={Link} to="/">
                  <HomeIcon />
                </Button>
                <Button color="inherit" component={Link} to="/login">
                  <VpnKey />
                </Button>
                <Menu />
              </div>
            </div>
          )}
        </Toolbar>
      </AppBar>
    );
  }
}

Navbar.propTypes = {
  authenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated,
});

export default connect(mapStateToProps)(Navbar);
