import React, { Component, Fragment } from "react";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

// MUI stuff
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import Typography from "@material-ui/core/Typography";
import Badge from "@material-ui/core/Badge";

// Icons
import ChatIcon from "@material-ui/icons/Chat";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Star from "@material-ui/icons/Star";
import People from "@material-ui/icons/People";
import Assignment from "@material-ui/icons/Assignment";

// Redux
import { connect } from "react-redux";
import { markNotificationsRead } from "../../redux/actions/userActions";

class Notifications extends Component {
  state = {
    anchorEl: null,
  };

  handleOpen = (event) => {
    this.setState({
      anchorEl: event.target,
    });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  // Send request to our backend to mark notifications read
  // We send an array of notification to backend which we need mark read
  onMenuOpened = () => {
    let unreadNotificationsIds = this.props.notifications
      .filter((not) => !not.read) // filter notifications that have "read: false"
      .map((not) => not.notificationId);
    this.props.markNotificationsRead(unreadNotificationsIds);
  };

  render() {
    const notifications = this.props.notifications;
    const anchorEl = this.state.anchorEl;

    dayjs.extend(relativeTime);

    let notificationsIcon;

    if (notifications && notifications.length > 0) {
      notifications.filter((not) => not.read === false).length > 0
        ? (notificationsIcon = (
            <Badge
              badgeContent={
                notifications.filter((not) => not.read === false).length
              }
              color="secondary"
            >
              <NotificationsIcon />
            </Badge>
          ))
        : (notificationsIcon = <NotificationsIcon />);
    } else {
      notificationsIcon = <NotificationsIcon />;
    }

    let notificationsMarkup =
      notifications && notifications.length > 0 ? (
        notifications.map((not) => {
          const verb =
            not.type === "like"
              ? " se sviđa tvoj oglas "
              : not.type === "comment"
              ? " komentira na tvoj oglas "
              : not.type === "follow"
              ? " te počinje pratiti "
              : " je objavio novi oglas ";
          const time = dayjs(not.createdAt).fromNow();
          const iconColor = not.read ? "primary" : "secondary";
          const icon =
            not.type === "like" ? (
              <Star
                color={iconColor}
                style={{ marginRight: 10, color: "#3c0557" }}
              />
            ) : not.type === "comment" ? (
              <ChatIcon
                color={iconColor}
                style={{ marginRight: 10, color: "#a1a603" }}
              />
            ) : not.type === "follow" ? (
              <People
                color={iconColor}
                style={{ marginRight: 10, color: "#025206" }}
              />
            ) : (
              <Assignment
                color={iconColor}
                style={{ marginRight: 10, color: "#213fff" }}
              />
            );
          return (
            <MenuItem key={not.createdAt} onClick={this.handleClose}>
              <div>
                <div>{icon}</div>
                <div>
                  <Typography
                    component={Link}
                    color="primary"
                    variant="body1"
                    to={`/users/${not.recipient}/card/${not.cardId}`}
                  >
                    <b>{not.sender}</b>
                    <p style={{ fontSize: 12 }}>{verb}</p>
                    <b style={{ color: "#b8b8b8", fontSize: 12 }}>{time}</b>
                    <p>_____________</p>
                  </Typography>
                </div>
              </div>
            </MenuItem>
          );
        })
      ) : (
        <MenuItem onClick={this.handleClose}>
          Trenutno nemate nikakvih obavijesti
        </MenuItem>
      );
    return (
      <Fragment>
        <Tooltip placement="top" title="Obavijesti">
          <IconButton
            aria-owns={anchorEl ? "simple-menu" : undefined}
            aria-haspopup="true"
            onClick={this.handleOpen}
          >
            {notificationsIcon}
          </IconButton>
        </Tooltip>
        <Menu
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
          onEntered={this.onMenuOpened}
        >
          {notificationsMarkup}
        </Menu>
      </Fragment>
    );
  }
}

Notifications.propTypes = {
  markNotificationsRead: PropTypes.func.isRequired,
  notifications: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  notifications: state.user.notifications,
});

export default connect(mapStateToProps, { markNotificationsRead })(
  Notifications
);
