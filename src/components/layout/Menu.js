import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import PopupState, { bindTrigger, bindMenu } from "material-ui-popup-state";
import MenuIcon from "@material-ui/icons/Menu";
import PropTypes from "prop-types";

// Icons
import HomeIcon from "@material-ui/icons/Home";
import ProfileIcon from "@material-ui/icons/Person";
import LogoutIcon from "@material-ui/icons/ExitToApp";
import LoginIcon from "@material-ui/icons/VpnKey";
import SignupIcon from "@material-ui/icons/ImportContacts";
import Business from "@material-ui/icons/Business";
import EmojiPeople from "@material-ui/icons/EmojiPeople";

// Redux
import { connect } from "react-redux";
import { logoutUser } from "../../redux/actions/userActions";

class MenuPopupState extends Component {
  handleHome = () => {
    window.location.href = "/";
  };

  handleLogin = () => {
    window.location.href = "/login";
  };

  handleSignup = () => {
    window.location.href = "/signup";
  };

  handleEmployers = () => {
    window.location.href = "/employers";
  };

  handleEmployees = () => {
    window.location.href = "/employees";
  };

  handleLogout = () => {
    this.props.logoutUser();
  };

  handleGoogleSign = () => {
    window.location.href = "/googlesign";
  };

  handleMyProfile = () => {
    const {
      user: {
        credentials: { username },
      },
    } = this.props;
    window.location.href = `/profile/${username}`;
  };
  render() {
    const {
      user: {
        authenticated,
        credentials: { username },
      },
    } = this.props;

    return (
      <PopupState variant="popover" popupId="demo-popup-menu">
        {(popupState) => (
          <React.Fragment>
            <Button {...bindTrigger(popupState)}>
              <MenuIcon />
            </Button>
            <Menu {...bindMenu(popupState)}>
              {authenticated ? (
                <MenuItem
                  onClick={this.handleMyProfile}
                  style={{
                    height: 20,
                  }}
                >
                  <p
                    style={{
                      color: "orange",
                      fontSize: 14,
                      marginLeft: 5,
                    }}
                  >
                    {username}
                  </p>
                </MenuItem>
              ) : null}
              <MenuItem onClick={this.handleHome}>
                <HomeIcon /> Naslovna
              </MenuItem>
              <MenuItem onClick={this.handleEmployers}>
                <Business /> Poslodavci
              </MenuItem>
              <MenuItem onClick={this.handleEmployees}>
                <EmojiPeople /> Posloprimci
              </MenuItem>
              {authenticated ? (
                <MenuItem onClick={this.handleMyProfile}>
                  {" "}
                  <ProfileIcon />
                  Moj profil
                </MenuItem>
              ) : null}

              {!authenticated ? (
                <MenuItem onClick={this.handleLogin}>
                  <LoginIcon /> Prijava
                </MenuItem>
              ) : null}
              {!authenticated ? (
                <MenuItem onClick={this.handleSignup}>
                  <SignupIcon /> Registracija
                </MenuItem>
              ) : null}
              {authenticated ? (
                <MenuItem onClick={this.handleLogout}>
                  <LogoutIcon />
                  Odjava
                </MenuItem>
              ) : null}
              {/*<MenuItem onClick={this.handleGoogleSign}>Google Sign</MenuItem>*/}
              <MenuItem>
                <div
                  style={{
                    height: "100%",
                    width: "100%",
                  }}
                >
                  <p
                    style={{
                      color: "#d4d5d6",
                      fontSize: 8,
                      width: "100%",
                      height: "100%",
                      textAlign: "right",
                    }}
                  >
                    Version - 1.3
                  </p>
                </div>
              </MenuItem>
            </Menu>
          </React.Fragment>
        )}
      </PopupState>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  authenticated: state.user.authenticated,
});

const mapActionsToProps = { logoutUser };

MenuPopupState.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  authenticated: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, mapActionsToProps)(MenuPopupState);
