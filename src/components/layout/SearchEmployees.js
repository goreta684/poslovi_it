import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";

// Redux stuff
import { connect } from "react-redux";

class SearchEmployees extends Component {
  handleSearchButton = () => {
    let user = document.getElementById("autocomplete-id").value;
    user = user.split("@");
    window.location.href = `/users/${user[1]}`;
    console.log(user);
  };
  render() {
    const { employees } = this.props;
    const {
      user: { authenticated },
      data: { loading },
    } = this.props;
    let searchUser =
      authenticated && !loading ? (
        <div style={{ width: "98%" }}>
          <Autocomplete
            freeSolo
            id="autocomplete-id"
            disableClearable
            options={employees.map(
              (option) => option.employerName + "  @" + option.username
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Pretraži posloprimce"
                margin="normal"
                variant="outlined"
                InputProps={{ ...params.InputProps, type: "search" }}
              />
            )}
          />
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleSearchButton}
          >
            Traži
          </Button>
        </div>
      ) : null;

    return <div>{searchUser}</div>;
  }
}

SearchEmployees.propTypes = {
  data: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  employees: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  data: state.data,
  user: state.user,
  employees: state.data.employees,
});

export default connect(mapStateToProps)(SearchEmployees);
