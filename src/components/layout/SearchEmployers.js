import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";

// Redux stuff
import { connect } from "react-redux";

class SearchEmployers extends Component {
  handleSearchButton = () => {
    let user = document.getElementById("autocomplete-id").value;
    user = user.split("@");
    window.location.href = `/users/${user[1]}`;
  };
  render() {
    const { employers } = this.props;
    const {
      user: { authenticated },
      data: { loading },
    } = this.props;
    let searchUser =
      authenticated && !loading ? (
        <div
          style={{
            width: "98%",
          }}
        >
          <Autocomplete
            freeSolo
            id="autocomplete-id"
            disableClearable
            options={employers.map(
              (option) => option.employerName + "  @" + option.username
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Pretraži poslodavce"
                margin="normal"
                variant="outlined"
                InputProps={{ ...params.InputProps, type: "search" }}
              />
            )}
          />
          <Button
            variant="contained"
            color="primary"
            onClick={this.handleSearchButton}
          >
            Traži
          </Button>
        </div>
      ) : null;

    return <div>{searchUser}</div>;
  }
}

SearchEmployers.propTypes = {
  data: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  employers: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  data: state.data,
  user: state.user,
  employers: state.data.employers,
});

export default connect(mapStateToProps)(SearchEmployers);
