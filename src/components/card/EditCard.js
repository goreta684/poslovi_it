import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";

//Icons
import EditIcon from "@material-ui/icons/Edit";

// MUI stuff
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";

// Redux stuff
import { connect } from "react-redux";
import {
  editPost,
  clearErrors,
  getCard,
} from "../../redux/actions/dataActions";

const styles = {
  button: {
    float: "right",
  },
};

class EditCard extends Component {
  state = {
    open: false,
    body: "",
    errors: {},
    index: 0,
  };

  mapCardDetailsToState = (cards) => {
    let i = 0;
    let index = 0;
    cards.forEach((card) => {
      if (card.cardId === this.props.cardId) {
        index = i;
      }
      i++;
    });
    this.setState({
      body: cards[index].body ? cards[index].body : "",
    });
  };

  componentDidMount() {
    const { cards } = this.props;
    this.mapCardDetailsToState(cards);
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.UI.errors) {
      this.setState({
        errors: nextProps.UI.errors,
      });
    }
    if (!nextProps.UI.errors && !nextProps.UI.loading) {
      this.setState({ body: "", open: false, errors: {} });
    }
  }
  handleOpen = () => {
    this.setState({ open: true });
    this.mapCardDetailsToState(this.props.cards);
  };
  handleClose = () => {
    this.props.clearErrors();
    this.setState({ open: false, errors: {} });
  };
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleSubmit = () => {
    const { cardId } = this.props;
    const postDetails = {
      body: this.state.body,
    };
    this.props.editPost(postDetails, cardId);
    this.handleClose();
    //window.location.href = "/";
  };
  render() {
    const { errors } = this.state;
    const { classes } = this.props;
    return (
      <Fragment>
        <Tooltip title="Uredi oglas" placement="top">
          <IconButton onClick={this.handleOpen} className={classes.button}>
            <EditIcon color="primary" />
          </IconButton>
        </Tooltip>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          fullWidth
          maxWidth="sm"
        >
          <DialogTitle>Edit your post</DialogTitle>
          <DialogContent>
            <form>
              <TextField
                name="body"
                type="text"
                label="Post"
                multiline
                rows="8"
                value={this.state.body}
                placeholder="Edit your post"
                error={errors.body ? true : false}
                helperText={errors.body}
                className={classes.textField}
                onChange={this.handleChange}
                fullWidth
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

EditCard.propTypes = {
  getCard: PropTypes.func.isRequired,
  editPost: PropTypes.func.isRequired,
  UI: PropTypes.object.isRequired,
  clearErrors: PropTypes.func.isRequired,
  cardId: PropTypes.string.isRequired,
  card: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  UI: state.UI,
  cards: state.data.cards,
  card: state.data.card,
});

export default connect(mapStateToProps, { editPost, clearErrors, getCard })(
  withStyles(styles)(EditCard)
);
