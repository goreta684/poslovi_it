import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";

//Icons
import DeleteOutline from "@material-ui/icons/DeleteOutline";

// MUI stuff
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";

// Redux stuff
import { connect } from "react-redux";
import { deleteComment } from "../../redux/actions/dataActions";

const styles = {
  button: {
    float: "right",
  },
  deleteButton: {
    position: "absolute",
    right: "1%",
    top: "1%",
    color: "#ffb74d",
  },
  deleteBtn: {
    color: "red",
    fontWeight: "bold",
  },
  cancelBtn: {
    fontWeight: "bold",
  },
};

class DeleteComment extends Component {
  state = {
    open: false,
  };

  handleOpen = () => {
    this.setState({
      open: true,
    });
  };

  handleClose = () => {
    this.setState({
      open: false,
    });
  };

  deleteComment = () => {
    this.props.deleteComment(this.props.cardId, this.props.commentId);
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    return (
      <Fragment>
        <Tooltip title="Izbriši komentar" placement="top">
          <IconButton onClick={this.handleOpen} className={classes.button}>
            <DeleteOutline color="primary" />
          </IconButton>
        </Tooltip>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          fullWidth
          maxWidth="sm"
        >
          <DialogTitle>
            Are you sure you want to delete this comment?
          </DialogTitle>
          <DialogActions>
            <Button
              onClick={this.handleClose}
              className={classes.cancelBtn}
              color="primary"
            >
              Cancel
            </Button>
            <Button onClick={this.deleteComment} className={classes.deleteBtn}>
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

DeleteComment.propTypes = {
  classes: PropTypes.object.isRequired,
  credentials: PropTypes.object.isRequired,
  deleteComment: PropTypes.func.isRequired,
  commentId: PropTypes.string,
  cardId: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  credentials: state.user.credentials,
});

export default connect(mapStateToProps, {
  deleteComment,
})(withStyles(styles)(DeleteComment));
