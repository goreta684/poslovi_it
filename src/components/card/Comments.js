import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import { Link } from "react-router-dom";
import dayjs from "dayjs";
import EditComment from "./EditComment";
import DeleteComment from "./DeleteComment";
import relativeTime from "dayjs/plugin/relativeTime";

// MUI stuff
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

// Redux stuff
import { connect } from "react-redux";

const styles = {
  commentImage: {
    width: 50,
    height: 50,
    objectFit: "cover",
    borderRadius: 50,
  },
  commentData: {
    flex: 1,
    margin: 10,
  },
  invisibleSeparator: {
    border: "none",
    margin: 4,
  },
  visibleSeparator: {
    width: "100%",
    borderBottom: "1px solid rgba(0,0,0,0.1)",
    marginBottom: 20,
  },
  containerGrid: {
    margin: 20,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  mainGrid: {
    margin: 20,
    width: "90%",
    display: "flex",
    flexDirection: "row",
  },
};

class Comments extends Component {
  formatPostBody = (body) => {
    let splittedBody = body.split("\\n");
    let finalBody = "";
    splittedBody.forEach((el) => {
      finalBody += "\n" + el;
    });
    return finalBody;
  };
  render() {
    dayjs.extend(relativeTime);
    const {
      comments,
      classes,
      user: {
        credentials: { username },
        authenticated,
      },
      card: { cardId },
    } = this.props;

    return (
      <Grid container className={classes.mainGrid}>
        {comments ? (
          comments.map((comment, index) => {
            const {
              body,
              createdAt,
              userImage,
              usernameHandle,
              commentId,
              updatedTime,
            } = comment;
            const editComments =
              authenticated && username === usernameHandle ? (
                <EditComment commentId={commentId} />
              ) : null;
            const deleteComments =
              authenticated && username === usernameHandle ? (
                <DeleteComment cardId={cardId} commentId={commentId} />
              ) : null;
            return (
              <Fragment key={createdAt}>
                <Grid
                  item
                  sm={12}
                  style={{
                    backgroundColor: "#ffffff",
                    borderRadius: 50,
                    justifyContent: "center",
                    marginTop: 5,
                    marginBottom: 5,
                    opacity: 0.8,
                    display: "flex",
                    flexDirection: "row",
                    width: "100%",
                  }}
                >
                  <Grid container className={classes.containerGrid}>
                    <Grid
                      item
                      sm={12}
                      style={{
                        width: "100%",
                        flex: 1,
                      }}
                    >
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                        }}
                      >
                        <div style={{ marginRight: 10 }}>
                          <img
                            src={userImage}
                            alt="slika komentatora"
                            className={classes.commentImage}
                          />
                        </div>
                        <div>
                          <Typography
                            variant="h5"
                            component={Link}
                            to={`/users/${usernameHandle}`}
                            color="primary"
                          >
                            {usernameHandle}
                          </Typography>
                          <Typography variant="body2" color="secondary">
                            {dayjs(createdAt).format("h:mm a, MMMM DD YYYY")}
                          </Typography>
                        </div>
                      </div>
                    </Grid>

                    <Grid
                      item
                      sm={12}
                      style={{
                        width: "100%",

                        flex: 1,
                      }}
                    >
                      <div className={classes.commentData}>
                        <hr className={classes.invisibleSeparator} />
                        <TextareaAutosize
                          value={this.formatPostBody(body)}
                          className={classes.body}
                          readOnly
                          style={{
                            width: "100%",
                            border: "none",
                            fontFamily: "Arial",
                            fontSize: 16,
                            textAlign: "justify",
                          }}
                        />
                        {updatedTime ? (
                          <Typography
                            style={{ color: "#bdbdbd", fontSize: 12 }}
                          >
                            Zadnji put ažurirano {dayjs(updatedTime).fromNow()}
                          </Typography>
                        ) : null}
                        {deleteComments}
                        {editComments}
                      </div>
                    </Grid>
                  </Grid>
                </Grid>
                {index !== comments.length - 1 && (
                  <hr className={classes.visibleSeparator} />
                )}
              </Fragment>
            );
          })
        ) : (
          <p>Komentari su trenutno nedostupni</p>
        )}
      </Grid>
    );
  }
}

Comments.propTypes = {
  comments: PropTypes.array, // with/without isRequired?
  authenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated,
  user: state.user,
  card: state.data.card,
});

export default connect(mapStateToProps)(withStyles(styles)(Comments));
