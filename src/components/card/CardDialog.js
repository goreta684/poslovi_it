import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import MyButton from "../../util/MyButton";
import dayjs from "dayjs";
import { Link } from "react-router-dom";
//import LikeButton from "./LikeButton";
import Comments from "./Comments";
import CommentForm from "./CommentForm";

// MUI stuff
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";
//Icons
import CloseIcon from "@material-ui/icons/Close";
import ChatIcon from "@material-ui/icons/ModeCommentOutlined";

// Redux stuff
import { connect } from "react-redux";
import { getCard, clearErrors } from "../../redux/actions/dataActions";

const styles = {
  gridContainer: {
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
  },
  invisibleSeparator: {
    border: "none",
    margin: 4,
  },
  visibleSeparator: {
    width: "100%",
    borderBottom: "1px solid rgba(0,0,0,0.1)",
    marginBottom: 20,
  },
  profileImage: {
    maxWidth: 100,
    maxHeight: 100,
    objectFit: "cover",
    borderRadius: "5%",
  },
  dialogContent: {
    padding: 20,
    display: "flex",
    flexDirection: "column",
  },
  expandButton: {
    //position: "absolute",
    //right: "1%",
    //bottom: "1%",
  },
  spinnerDiv: {
    textAlign: "center",
    marginTop: 50,
    marginBottom: 50,
  },
};

class CardDialog extends Component {
  state = {
    open: false,
    oldPath: "",
    newPath: "",
  };

  componentDidMount() {
    if (this.props.openDialog) {
      this.handleOpen();
    }
  }

  handleOpen = () => {
    let oldPath = window.location.pathname;

    const { usernameHandle, cardId } = this.props;
    const newPath = `/users/${usernameHandle}/card/${cardId}`;

    if (oldPath === newPath) oldPath = `/users/${usernameHandle}`;

    window.history.pushState(null, null, newPath);

    this.setState({ open: true, oldPath, newPath });
    this.props.getCard(this.props.cardId);
  };
  handleClose = () => {
    window.history.pushState(null, null, this.state.oldPath);
    this.setState({ open: false });
    this.props.clearErrors();
  };

  render() {
    const {
      classes,
      card: { cardId, body, createdAt, userImage, usernameHandle, comments },
      UI: { loading },
    } = this.props;

    const dialogMarkup = loading ? (
      <div className={classes.spinnerDiv}>
        <CircularProgress size={200} thickness={2} />
      </div>
    ) : (
      <Grid container spacing={10} className={classes.gridContainer}>
        <Grid
          item
          sm={5}
          className={classes.gridItem1}
          style={{
            width: "100%",
            textAlign: "center",
            flex: 1,
          }}
        >
          <img src={userImage} alt="Profile" className={classes.profileImage} />
        </Grid>
        <Grid
          item
          sm={10}
          className={classes.gridItem2}
          style={{ width: "100%", flex: 4 }}
        >
          <Typography
            component={Link}
            color="primary"
            variant="h5"
            to={`/users/${usernameHandle}`}
          >
            @{usernameHandle}
          </Typography>
          <hr className={classes.invisibleSeparator} />
          <Typography variant="body2" style={{ color: "#d9d8cc" }}>
            {dayjs(createdAt).format("h:mm a, MMMM DD YYYY")}
          </Typography>
          <hr className={classes.invisibleSeparator} />
          <TextareaAutosize
            value={body}
            className={classes.body}
            readOnly
            style={{
              width: "100%",
              border: "none",
              color: "black",
              textAlign: "justify",
              fontFamily: "Arial",
              fontSize: 16,
            }}
          />
        </Grid>
        <hr className={classes.visibleSeparator} />
        <CommentForm cardId={cardId} />
        <Comments comments={comments} />
      </Grid>
    );

    return (
      <Fragment>
        <MyButton
          onClick={this.handleOpen}
          tip="Komentari"
          tipClassName={classes.expandButton}
        >
          <ChatIcon color="primary" />
        </MyButton>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          fullWidth
          maxWidth="sm"
        >
          <MyButton
            tip="Zatvori"
            onClick={this.handleClose}
            tipClassName={classes.closeButton}
          >
            <CloseIcon />
          </MyButton>
          <DialogContent className={classes.dialogContent}>
            {dialogMarkup}
          </DialogContent>
        </Dialog>
      </Fragment>
    );
  }
}

CardDialog.propsTypes = {
  clearErrors: PropTypes.func.isRequired,
  getCard: PropTypes.func.isRequired,
  cardId: PropTypes.string.isRequired,
  usernameHandle: PropTypes.string.isRequired,
  card: PropTypes.object.isRequired,
  UI: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  card: state.data.card,
  UI: state.UI,
});

const mapActionsToProps = {
  getCard,
  clearErrors,
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(withStyles(styles)(CardDialog));
