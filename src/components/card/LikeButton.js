import React, { Component } from "react";
import MyButton from "../../util/MyButton";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

// Icons
import Star from "@material-ui/icons/Star";
import StarBorder from "@material-ui/icons/StarBorder";

// Redux stuff
import { connect } from "react-redux";
import { likeCard, unlikeCard } from "../../redux/actions/dataActions";

export class LikeButton extends Component {
  likedCard = () => {
    if (
      this.props.user.likes &&
      this.props.user.likes.find((like) => like.cardId === this.props.cardId)
    )
      return true;
    else return false;
  };

  likeCard = () => {
    this.props.likeCard(this.props.cardId);
  };

  unlikeCard = () => {
    this.props.unlikeCard(this.props.cardId);
  };
  render() {
    const { authenticated } = this.props.user;

    const likeButton = !authenticated ? (
      <Link to="/login">
        <MyButton tip="Sviđa mi se">
          <StarBorder color="primary" />
        </MyButton>
      </Link>
    ) : this.likedCard() ? (
      <MyButton tip="Ne sviđa mi se" onClick={this.unlikeCard}>
        <Star color="primary" />
      </MyButton>
    ) : (
      <MyButton tip="Sviđa mi se" onClick={this.likeCard}>
        <StarBorder color="primary" />
      </MyButton>
    );
    return likeButton;
  }
}

LikeButton.propTypes = {
  user: PropTypes.object.isRequired,
  cardId: PropTypes.string.isRequired,
  likeCard: PropTypes.func.isRequired,
  unlikeCard: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapActionsToProps = {
  likeCard,
  unlikeCard,
};

export default connect(mapStateToProps, mapActionsToProps)(LikeButton);
