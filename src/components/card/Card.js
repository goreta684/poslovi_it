import React, { Component } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import PropTypes from "prop-types";
//import MyButton from "../../util/MyButton";
import DeleteCard from "./DeleteCard";
import CardDialog from "./CardDialog";
import LikeButton from "./LikeButton";
import EditCard from "./EditCard";

//MUI stuff
import CardContainer from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import TextareaAutosize from "@material-ui/core/TextareaAutosize";

// Icons
//import ChatIcon from "@material-ui/icons/ModeCommentOutlined";

//Redux
import { connect } from "react-redux";

const Link = require("react-router-dom").Link;

const styles = {
  card: {
    position: "relative",
    flexDirection: "row",
    marginBottom: 20,
  },
  image: {
    minWidth: 50,
    maxWidth: 50,
    minHeight: 50,
    maxHeight: 50,
    margin: 10,
    borderRadius: 5,
  },
  content: {
    padding: 5,
    objectFit: "cover",
    margin: 10,
  },
  body: {
    margin: 10,
    maxWidth: "90%",
    textAlign: "justify",
  },
  deleteCard: {
    //right: "10%",
  },
};

class Card extends Component {
  formatPostBody = (body) => {
    let splittedBody = body.split("\\n");
    let finalBody = "";
    splittedBody.forEach((el) => {
      finalBody += "\n" + el;
    });
    return finalBody;
  };
  render() {
    let trimString = function (string, length) {
      return string.length > length
        ? string.substring(0, length) + " ..."
        : string;
    };
    dayjs.extend(relativeTime);
    const {
      classes,
      card: {
        body,
        createdAt,
        usernameHandle,
        employerNameHandle,
        userImage,
        cardId,
        likeCount,
        commentCount,
        updatedTime,
      },
      user: {
        authenticated,
        credentials: { username },
      },
    } = this.props;

    const deleteButton =
      authenticated && usernameHandle === username ? (
        <DeleteCard cardId={cardId} className={classes.deleteCard} />
      ) : null;

    const editCardButton =
      authenticated && usernameHandle === username ? (
        <EditCard cardId={cardId} />
      ) : null;
    return (
      <CardContainer className={classes.card}>
        <CardMedia
          image={userImage}
          title="Slika profila"
          className={classes.image}
          component={Link}
          to={`/users/${usernameHandle}`}
        />
        <CardContent className={classes.content}>
          <Typography
            variant="h5"
            component={Link}
            to={`/users/${usernameHandle}`}
            color="primary"
          >
            {employerNameHandle}
          </Typography>
          {deleteButton}
          {editCardButton}
          <Typography variant="body2" color="textSecondary">
            {dayjs(createdAt).fromNow()}
          </Typography>
          <TextareaAutosize
            value={trimString(this.formatPostBody(body), 600)}
            className={classes.body}
            readOnly
            style={{
              width: "90%",
              border: "none",
              fontFamily: "Arial",
              fontSize: 16,
              textAlign: "justify",
            }}
          />
          {updatedTime ? (
            <Typography style={{ color: "#bdbdbd", fontSize: 12 }}>
              Zadnji put ažurirano {dayjs(updatedTime).fromNow()}
            </Typography>
          ) : null}
          <div
            style={{
              width: "70%",
            }}
          >
            <LikeButton cardId={cardId} />
            <span>{likeCount}</span>
          </div>

          <CardDialog
            cardId={cardId}
            usernameHandle={usernameHandle}
            openDialog={this.props.openDialog}
          />
          <span>{commentCount}</span>
        </CardContent>
      </CardContainer>
    );
  }
}

Card.propTypes = {
  user: PropTypes.object.isRequired,
  card: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  openDialog: PropTypes.bool,
};

const mapStateToProps = (state) => ({
  user: state.user,
});

export default connect(mapStateToProps)(withStyles(styles)(Card));
