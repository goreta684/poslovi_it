import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";

//Icons
import EditIcon from "@material-ui/icons/Edit";

// MUI stuff
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";

import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";

// Redux stuff
import { connect } from "react-redux";
import {
  editComment,
  clearErrors,
  getCard,
} from "../../redux/actions/dataActions";

const styles = {
  button: {
    float: "right",
  },
};

class EditComment extends Component {
  state = {
    open: false,
    body: "",
    errors: {},
  };
  mapCardDetailsToState = (comments) => {
    let i = 0;
    let index = 0;
    comments.forEach((c) => {
      if (c.commentId === this.props.commentId) {
        index = i;
      }
      i++;
    });
    this.setState({
      body: comments[index].body ? comments[index].body : "",
    });
  };

  componentDidMount() {
    const {
      card: { comments },
    } = this.props;
    this.mapCardDetailsToState(comments);
  }

  handleOpen = () => {
    const {
      card: { comments },
    } = this.props;
    this.setState({ open: true });
    this.mapCardDetailsToState(comments);
  };
  handleClose = () => {
    this.props.clearErrors();
    this.setState({ open: false, errors: {} });
  };
  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleSubmit = () => {
    const { commentId } = this.props;
    const commDetails = {
      body: this.state.body,
    };
    this.props.editComment(commDetails, commentId);
    this.handleClose();
  };
  render() {
    const { errors } = this.state;
    const { classes } = this.props;
    return (
      <Fragment>
        <Tooltip title="Uredi oglas" placement="top">
          <IconButton onClick={this.handleOpen} className={classes.button}>
            <EditIcon color="primary" />
          </IconButton>
        </Tooltip>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          fullWidth
          maxWidth="sm"
        >
          <DialogTitle>Edit your comment</DialogTitle>
          <DialogContent>
            <form>
              <TextField
                name="body"
                type="text"
                label="Comment"
                multiline
                rows="8"
                value={this.state.body}
                placeholder="Edit your comment"
                error={errors.body ? true : false}
                helperText={errors.body}
                className={classes.textField}
                onChange={this.handleChange}
                fullWidth
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

EditComment.propTypes = {
  getCard: PropTypes.func.isRequired,
  editComment: PropTypes.func.isRequired,
  UI: PropTypes.object.isRequired,
  clearErrors: PropTypes.func.isRequired,
  card: PropTypes.object.isRequired,
  commentId: PropTypes.string,
};

const mapStateToProps = (state) => ({
  UI: state.UI,
  cards: state.data.cards,
  card: state.data.card,
  comments: state.data.card.comments,
});

export default connect(mapStateToProps, { editComment, clearErrors, getCard })(
  withStyles(styles)(EditComment)
);
