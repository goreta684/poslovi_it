import React, { Component, Fragment } from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import PropTypes from "prop-types";
import MyButton from "../../util/MyButton";

// MUI stuff
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";

//Icons
import DeleteOutline from "@material-ui/icons/DeleteOutline";

// Redux stuff
import { connect } from "react-redux";
import { deleteCard } from "../../redux/actions/dataActions";

const styles = {
  deleteButton: {
    position: "absolute",
    right: "1%",
    top: "1%",
    color: "#ffb74d",
  },
  deleteBtn: {
    color: "red",
    fontWeight: "bold",
  },
  cancelBtn: {
    fontWeight: "bold",
  },
};

class DeleteCard extends Component {
  state = {
    open: false,
  };
  handleOpen = () => {
    this.setState({
      open: true,
    });
  };
  handleClose = () => {
    this.setState({
      open: false,
    });
  };
  deleteCard = () => {
    this.props.deleteCard(this.props.cardId);
    this.setState({ open: false });
  };
  render() {
    const { classes } = this.props;

    return (
      <Fragment>
        <MyButton
          tip="Izbriši oglas"
          onClick={this.handleOpen}
          btnClassName={classes.deleteButton}
        >
          <DeleteOutline />
        </MyButton>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          fullWidth
          maxWidth="sm"
        >
          <DialogTitle>Are you sure you want to delete this post?</DialogTitle>
          <DialogActions>
            <Button
              onClick={this.handleClose}
              className={classes.cancelBtn}
              color="primary"
            >
              Cancel
            </Button>
            <Button onClick={this.deleteCard} className={classes.deleteBtn}>
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </Fragment>
    );
  }
}

DeleteCard.propTypes = {
  classes: PropTypes.object.isRequired,
  deleteCard: PropTypes.func.isRequired,
  cardId: PropTypes.string.isRequired,
};

export default connect(null, { deleteCard })(withStyles(styles)(DeleteCard));
