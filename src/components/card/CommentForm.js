import React, { Component } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";

// MUI stuff
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

// Redux stuff
import { connect } from "react-redux";
import { submitComment } from "../../redux/actions/dataActions";

const styles = {
  button: {
    marginTop: 20,
    marginBottom: 0,
  },
};

class CommentForm extends Component {
  state = {
    body: "",
    errors: {},
  };

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.UI.errors) {
      this.setState({ errors: nextProps.UI.errors });
    }
    if (!nextProps.UI.errors && !nextProps.UI.loading) {
      this.setState({ body: "" });
    }
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.props.submitComment(this.props.cardId, { body: this.state.body });
    //window.location.reload(false);
  };
  render() {
    const { classes, authenticated } = this.props;
    const errors = this.state.errors;

    const commentMarkup = authenticated ? (
      <Grid
        item
        sm={12}
        style={{
          textAlign: "center",
          width: "100%",
          backgroundColor: "#fff",
        }}
      >
        <form onSubmit={this.handleSubmit}>
          <TextField
            name="body"
            type="text"
            multiline
            variant="outlined"
            row={3}
            label="Komentar na oglas"
            error={errors.comment ? true : false}
            helperText={errors.comment}
            value={this.state.body}
            onChange={this.handleChange}
            fullWidth
            className={classes.TextField}
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            className={classes.button}
          >
            Komentiraj
          </Button>
        </form>
      </Grid>
    ) : null;
    return commentMarkup;
  }
}

CommentForm.propTypes = {
  cardId: PropTypes.string,
  UI: PropTypes.object.isRequired,
  submitComment: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  authenticated: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated,
  UI: state.UI,
});

export default connect(mapStateToProps, { submitComment })(
  withStyles(styles)(CommentForm)
);
