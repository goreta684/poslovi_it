import React, { Fragment } from "react";
import PropTypes from "prop-types";

//MUI stuff
import withStyles from "@material-ui/core/styles/withStyles";
import CardContainer from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Computer from "@material-ui/icons/Computer";

import AboutIcon from "@material-ui/icons/Info";

import dayjs from "dayjs";

const Link = require("react-router-dom").Link;

const styles = {
  card: {
    marginTop: 20,
    position: "relative",
    display: "flex",
    flexDirection: "row",
    marginBottom: 20,
  },
  image: {
    minWidth: 100,
    maxWidth: 200,
    minHeight: 100,
    maxHeight: 100,
    margin: 10,
    borderRadius: 5,
  },
  content: {
    padding: 5,
    objectFit: "cover",
    margin: 10,
    width: "100%",
  },
  body: {
    margin: 10,
    maxWidth: "90%",
    textAlign: "justify",
  },
};

let trimString = function (string, length) {
  return string.length > length ? string.substring(0, length) + " ..." : string;
};

const Employee = (props) => {
  const {
    classes,
    employee: {
      imageUrl,
      username,
      employerName,
      createdAt,
      description,
      other,
    },
  } = props;

  return (
    <CardContainer className={classes.card}>
      <CardMedia
        image={imageUrl}
        title="Slika profila"
        className={classes.image}
        component={Link}
        to={`/users/${username}`}
      />
      <CardContent className={classes.content}>
        <Fragment>
          <Typography
            variant="h5"
            component={Link}
            to={`/users/${username}`}
            color="primary"
          >
            {employerName}
          </Typography>
          <hr />
        </Fragment>

        <Fragment>
          {description ? (
            <Fragment>
              <AboutIcon style={{ verticalAlign: "middle" }} color="primary" />
              {trimString(description, 600)}
            </Fragment>
          ) : (
            <p style={{ color: "#c9c7c3" }}>Opis nije dodan</p>
          )}
        </Fragment>
        <Fragment>
          {other ? (
            <Fragment>
              <br />
              <Computer style={{ verticalAlign: "middle" }} color="primary" />
              {trimString(other, 600)}
            </Fragment>
          ) : (
            <p style={{ color: "#c9c7c3" }}>Tehnologije nisu dodane</p>
          )}
        </Fragment>
        <Typography
          style={{
            margin: 10,
            textAlign: "right",
            color: "#e0dfdc",
          }}
        >
          Joined {dayjs(createdAt).format("MMM YYYY")}
        </Typography>
      </CardContent>
    </CardContainer>
  );
};

Employee.propTypes = {
  classes: PropTypes.object.isRequired,
  employee: PropTypes.object.isRequired,
};

export default withStyles(styles)(Employee);
