import React, { Fragment } from "react";
import PropTypes from "prop-types";
import dayjs from "dayjs";

//MUI Stuff
import withStyles from "@material-ui/core/styles/withStyles";
import CardContainer from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import LocationOn from "@material-ui/icons/LocationOn";
import LinkIcon from "@material-ui/icons/Link";
import Computer from "@material-ui/icons/Computer";

const Link = require("react-router-dom").Link;

const styles = {
  card: {
    marginTop: 20,
    position: "relative",
    display: "flex",
    flexDirection: "row",
    marginBottom: 20,
  },
  image: {
    minWidth: 100,
    maxWidth: 200,
    minHeight: 100,
    maxHeight: 100,
    margin: 10,
    borderRadius: 5,
  },
  content: {
    padding: 5,
    objectFit: "cover",
    margin: 10,
    width: "95%",
  },
  body: {
    margin: 10,
    maxWidth: "90%",
    textAlign: "justify",
  },
};

let trimString = function (string, length) {
  return string.length > length ? string.substring(0, length) + " ..." : string;
};

const Employer = (props) => {
  const {
    classes,
    employer: {
      imageUrl,
      username,
      employerName,
      website,
      createdAt,
      location,
      //git,
      //linkedin,
      //socialNetwork,
      other,
    },
  } = props;

  return (
    <CardContainer className={classes.card}>
      <CardMedia
        image={imageUrl}
        title="Slika profila"
        className={classes.image}
        component={Link}
        to={`/users/${username}`}
      />
      <CardContent className={classes.content}>
        <Fragment>
          <Typography
            variant="h5"
            component={Link}
            to={`/users/${username}`}
            color="primary"
          >
            <p>{employerName}</p>
          </Typography>
          <hr />
        </Fragment>

        <Fragment>
          {location ? (
            <Fragment>
              <LocationOn style={{ verticalAlign: "middle" }} color="primary" />
              {location}
              <br />
            </Fragment>
          ) : (
            <p style={{ color: "#c9c7c3" }}>Lokacija nije dodana</p>
          )}
        </Fragment>
        <Fragment>
          {website ? (
            <Fragment>
              <LinkIcon style={{ verticalAlign: "middle" }} color="primary" />
              <a href={website} target="_blank" rel="noopener noreferrer">
                {" "}
                {website}
              </a>
            </Fragment>
          ) : (
            <p style={{ color: "#c9c7c3" }}>Webstranica nije dodana</p>
          )}
        </Fragment>
        <Fragment>
          {other ? (
            <Fragment>
              <br />
              <Computer style={{ verticalAlign: "middle" }} color="primary" />
              {trimString(other, 600)}
            </Fragment>
          ) : (
            <p style={{ color: "#c9c7c3" }}>Tehnologije nisu dodane</p>
          )}
        </Fragment>
        <Typography
          style={{
            margin: 10,
            textAlign: "right",
            color: "#e0dfdc",
          }}
        >
          Joined {dayjs(createdAt).format("MMM YYYY")}
        </Typography>
      </CardContent>
    </CardContainer>
  );
};

Employer.propTypes = {
  classes: PropTypes.object.isRequired,
  employer: PropTypes.object.isRequired,
};

export default withStyles(styles)(Employer);
