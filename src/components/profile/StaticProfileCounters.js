import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import fetch from "node-fetch";

// MUI stuff
import Typography from "@material-ui/core/Typography";

// Redux
import { connect } from "react-redux";
import { getUserData } from "../../redux/actions/dataActions";

const sendRequest = async (url, params = undefined) => {
  const bearerToken = localStorage.getItem("FBIdToken");
  return await fetch(url, {
    method: "GET",
    mode: "cors",
    headers: {
      Authorization: bearerToken,
      "Content-Type": "application/json",
    },
    credentials: "include",
    withCredentials: true,
  });
};

export class StaticProfileCounters extends Component {
  state = {
    followers: null,
    following: null,
    postCount: null,
  };

  componentDidMount() {
    const { username } = this.props;
    this.props.getUserData(username);

    sendRequest(`/user/${username}`)
      .then((res) => res.json())
      .then((res) => {
        this.setState({
          followers: res.user.followers,
          following: res.user.following,
          postCount: res.cards.length,
        });
      })
      .catch((err) => console.log(err));

    /*axios
      .get(`/user/${username}`)
      .then((res) => {
        this.setState({
          followers: res.data.user.followers,
          following: res.data.user.following,
          postCount: res.data.cards.length,
        });
      })
      .catch((err) => console.log(err));*/
  }

  render() {
    const { followers, following, postCount } = this.state;
    const { authenticated } = this.props;

    return authenticated &&
      followers !== null &&
      following !== null &&
      postCount !== null ? (
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <div>
          <Typography style={{ fontSize: 20, fontFamily: "Courier New" }}>
            {" "}
            <b>{postCount}</b>
          </Typography>
          <Typography style={{ fontSize: 16, padding: 4, color: "#787a79" }}>
            Oglasi
          </Typography>
        </div>
        <div>
          <Typography style={{ fontSize: 20, fontFamily: "Courier New" }}>
            <b>{followers}</b>
          </Typography>
          <Typography style={{ fontSize: 16, padding: 4, color: "#787a79" }}>
            Pratitelji
          </Typography>
        </div>
        <div>
          <Typography style={{ fontSize: 20, fontFamily: "Courier New" }}>
            {" "}
            <b>{following}</b>
          </Typography>
          <Typography style={{ fontSize: 16, padding: 4, color: "#787a79" }}>
            Prati
          </Typography>
        </div>
      </div>
    ) : null;
  }
}

StaticProfileCounters.propTypes = {
  username: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated,
});

export default connect(mapStateToProps, { getUserData })(StaticProfileCounters);
