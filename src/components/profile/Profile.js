import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import dayjs from "dayjs";
import EditDetails from "./EditDetails";
import MyButton from "../../util/MyButton";

//MUI Stuff
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";
import MuiLink from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import CircularProgress from "@material-ui/core/CircularProgress";
import Grid from "@material-ui/core/Grid";

//Icons
import LocationOn from "@material-ui/icons/LocationOn";
import LinkIcon from "@material-ui/icons/Link";
import CalendarToday from "@material-ui/icons/CalendarToday";
import EmailIcon from "@material-ui/icons/Email";
import ContactIcon from "@material-ui/icons/LocalPhoneTwoTone";
import AboutIcon from "@material-ui/icons/Info";
import EditIcon from "@material-ui/icons/Edit";
import Computer from "@material-ui/icons/Computer";

// Redux
import { connect } from "react-redux";
import { logoutUser, uploadImage } from "../../redux/actions/userActions";

const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    marginTop: 120,
  },
  image: {
    flex: 1,
    textAlign: "center",
  },
  about: {
    flex: 1,
  },
  credentials: {
    margin: 20,
  },
  span: {
    verticalAlign: "middle",
  },
  paper: {
    padding: 20,
  },
  buttons: {
    textAlign: "center",
  },
};

class Profile extends Component {
  state = {
    flag: false,
  };
  handleImageChange = (event) => {
    const image = event.target.files[0];
    const formData = new FormData();
    formData.append("image", image, image.name);
    this.props.uploadImage(formData);
  };

  handleEditPicture = () => {
    const fileInput = document.getElementById("imageInput");
    fileInput.click();
  };

  handleLogout = () => {
    this.props.logoutUser();
  };

  handleFollowers = () => {
    this.setState({
      flag: true,
    });
  };

  handleFollowing = () => {
    this.setState({
      flag: false,
    });
  };

  render() {
    const {
      classes,
      user: {
        credentials: {
          username,
          createdAt,
          imageUrl,
          description,
          website,
          location,
          employerName,
          //oib,
          email,
          contact,
          git,
          linkedin,
          socialNetwork,
          other,
          //following,
          //followers
        },
        followers,
        following,
        loading,
        authenticated,
      },
    } = this.props;

    const flw = followers;
    const flg = following;
    let profileMarkup = !loading ? (
      authenticated ? (
        <div className={classes.container}>
          <div className={classes.image}>
            <img
              src={imageUrl}
              alt="Profile"
              style={{
                maxWidth: 300,
                maxHeight: 300,
                margin: 10,
                borderRadius: 20,
              }}
            />
            <input
              type="file"
              id="imageInput"
              hidden="hidden"
              onChange={this.handleImageChange}
            />
            <MyButton
              tip="Edit profile picture"
              onClick={this.handleEditPicture}
              btnClassName="button"
            >
              <EditIcon color="primary" />
            </MyButton>
          </div>
          <div className={classes.about}>
            <div className={classes.credentials}>
              <div style={{ marginBottom: 20 }}>
                <MuiLink
                  component={Link}
                  to={`/users/${username}`}
                  color="primary"
                  variant="h4"
                >
                  @{username}
                </MuiLink>
              </div>
              {employerName && (
                <div>
                  <Typography style={{ fontSize: 30 }} variant="body2">
                    {employerName}
                  </Typography>
                  <hr />
                </div>
              )}
              {email && (
                <Fragment>
                  <EmailIcon className={classes.span} color="primary" />{" "}
                  <span>{email}</span>
                  <hr />
                </Fragment>
              )}
              {contact && (
                <Fragment>
                  <ContactIcon className={classes.span} color="primary" />{" "}
                  <span>{contact}</span>
                  <hr />
                </Fragment>
              )}
              {description && (
                <Fragment>
                  <AboutIcon className={classes.span} color="primary" />{" "}
                  <p style={{ textAlign: "justify" }}>{description}</p>
                  <hr />
                </Fragment>
              )}
              {location && (
                <Fragment>
                  <LocationOn className={classes.span} color="primary" />{" "}
                  <span>{location}</span>
                  <hr />
                </Fragment>
              )}
              {website && (
                <Fragment>
                  <LinkIcon className={classes.span} color="primary" />
                  <a href={website} target="_blank" rel="noopener noreferrer">
                    {" "}
                    {website}
                  </a>
                  <hr />
                </Fragment>
              )}
              {git && (
                <Fragment>
                  <LinkIcon className={classes.span} color="primary" />
                  <a href={git} target="_blank" rel="noopener noreferrer">
                    {" "}
                    {git}
                  </a>
                  <hr />
                </Fragment>
              )}
              {linkedin && (
                <Fragment>
                  <LinkIcon className={classes.span} color="primary" />
                  <a href={linkedin} target="_blank" rel="noopener noreferrer">
                    {" "}
                    {linkedin}
                  </a>
                  <hr />
                </Fragment>
              )}
              {socialNetwork && (
                <Fragment>
                  <LinkIcon className={classes.span} color="primary" />
                  <a
                    href={socialNetwork}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    {" "}
                    {socialNetwork}
                  </a>
                  <hr />
                </Fragment>
              )}
              {other && (
                <Fragment>
                  <Computer className={classes.span} color="primary" />
                  {other}
                  <hr />
                </Fragment>
              )}
              <CalendarToday className={classes.span} color="primary" />{" "}
              <span>Joined {dayjs(createdAt).format("MMM YYYY")}</span>
              <EditDetails />
            </div>

            <div
              style={{
                textAlign: "center",
                display: "flex",
                flexDirection: "row",
                width: "100%",
              }}
            >
              <Button
                style={{
                  backgroundColor: this.state.flag ? "#edebe8" : "#f09337",
                  flex: 1,
                }}
                onClick={this.handleFollowing}
              >
                Pratitelji ( {flw.length} )
              </Button>
              <Button
                style={{
                  backgroundColor: this.state.flag ? "#f09337" : "#edebe8",
                  flex: 1,
                }}
                onClick={this.handleFollowers}
              >
                Pratim ( {flg.length} )
              </Button>
            </div>

            <div
              style={{
                display: "flex",
                flexDirection: "row",
                marginTop: "100px",
                textAlign: "center",
                marginLeft: 20,
                marginRight: 20,
              }}
            >
              {!this.state.flag ? (
                <div
                  style={{
                    flex: 1,
                    // marginRight: 10,
                  }}
                >
                  {flw.map((f, i) => (
                    <div key={i}>
                      <MuiLink
                        component={Link}
                        to={`/users/${f.sender}`}
                        color="primary"
                        variant="h6"
                      >
                        {f.sender}
                      </MuiLink>
                      <hr />
                    </div>
                  ))}
                </div>
              ) : (
                <div style={{ flex: 1 }}>
                  {flg.map((f, i) => (
                    <div key={i}>
                      <MuiLink
                        component={Link}
                        to={`/users/${f.recipient}`}
                        color="secondary"
                        variant="h6"
                      >
                        {f.recipient}
                      </MuiLink>
                      <hr />
                    </div>
                  ))}
                </div>
              )}
            </div>
          </div>
        </div>
      ) : (
        <Paper className={classes.paper}>
          <Typography variant="body2" align="center" color="secondary">
            Niste prijavljeni, molimo prijavite se ...
          </Typography>
          <div className={classes.buttons}>
            <Button
              variant="contained"
              color="primary"
              component={Link}
              to="/login"
              style={{ margin: 20 }}
            >
              Prijava
            </Button>
            <Button
              variant="contained"
              color="primary"
              component={Link}
              to="/signup"
            >
              Registracija
            </Button>
          </div>
        </Paper>
      )
    ) : (
      <Grid
        container
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Grid item sm />
        <Grid item sm>
          <CircularProgress size={200} style={{ marginTop: "30%" }} />
        </Grid>
        <Grid item sm />
      </Grid>
    );
    return profileMarkup;
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapActionsToProps = { logoutUser, uploadImage };

Profile.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(withStyles(styles)(Profile));
