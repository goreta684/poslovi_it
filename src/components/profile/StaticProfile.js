import React, { Fragment } from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import dayjs from "dayjs";
import FollowComponent from "./Follow";
import StaticProfileCounters from "./StaticProfileCounters";

// MUI stuff
import Typography from "@material-ui/core/Typography";

//Icons
import LocationOn from "@material-ui/icons/LocationOn";
import LinkIcon from "@material-ui/icons/Link";
import CalendarToday from "@material-ui/icons/CalendarToday";
import EmailIcon from "@material-ui/icons/Email";
import ContactIcon from "@material-ui/icons/LocalPhoneTwoTone";
import AboutIcon from "@material-ui/icons/Info";
import Computer from "@material-ui/icons/Computer";

const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
  },
  image: {
    flex: 1,
    textAlign: "center",
  },
  about: {
    flex: 1,
  },
  credentials: {
    margin: 20,
  },
  span: {
    verticalAlign: "middle",
  },
};

const StaticProfile = (props) => {
  const {
    classes,
    profile: {
      username,
      createdAt,
      imageUrl,
      description,
      website,
      location,
      email,
      employerName,
      //oib,
      contact,
      git,
      linkedin,
      socialNetwork,
      other,
    },
  } = props;

  return (
    <div className={classes.container}>
      <div className={classes.image}>
        <img
          src={imageUrl}
          alt="Profile"
          style={{
            maxWidth: 250,
            maxHeight: 250,
            margin: 10,
            borderRadius: 20,
          }}
        />
      </div>
      <div className={classes.about}>
        <div className={classes.credentials}>
          <div style={{ margin: 30, textAlign: "center" }}>
            <Typography
              style={{ fontSize: 30, color: "orange", marginBottom: 10 }}
            >
              @{username}
            </Typography>
            <StaticProfileCounters username={username} />
          </div>
          <FollowComponent username={username} user={username} />
          {employerName && (
            <div>
              <Typography style={{ fontSize: 30 }} variant="body2">
                {employerName}
              </Typography>
              <hr />
            </div>
          )}
          {email && (
            <Fragment>
              <EmailIcon className={classes.span} color="primary" />{" "}
              <span>{email}</span>
              <hr />
            </Fragment>
          )}
          {contact && (
            <Fragment>
              <ContactIcon className={classes.span} color="primary" />{" "}
              <span>{contact}</span>
              <hr />
            </Fragment>
          )}
          {description && (
            <Fragment>
              <AboutIcon className={classes.span} color="primary" />{" "}
              <p style={{ textAlign: "justify" }}>{description}</p>
              <hr />
            </Fragment>
          )}
          {location && (
            <Fragment>
              <LocationOn className={classes.span} color="primary" />{" "}
              <span>{location}</span>
              <hr />
            </Fragment>
          )}
          {website && (
            <Fragment>
              <LinkIcon className={classes.span} color="primary" />
              <a href={website} target="_blank" rel="noopener noreferrer">
                {" "}
                {website}
              </a>
              <hr />
            </Fragment>
          )}
          {git && (
            <Fragment>
              <LinkIcon className={classes.span} color="primary" />
              <a href={git} target="_blank" rel="noopener noreferrer">
                {" "}
                {git}
              </a>
              <hr />
            </Fragment>
          )}
          {linkedin && (
            <Fragment>
              <LinkIcon className={classes.span} color="primary" />
              <a href={linkedin} target="_blank" rel="noopener noreferrer">
                {" "}
                {linkedin}
              </a>
              <hr />
            </Fragment>
          )}
          {socialNetwork && (
            <Fragment>
              <LinkIcon className={classes.span} color="primary" />
              <a href={socialNetwork} target="_blank" rel="noopener noreferrer">
                {" "}
                {socialNetwork}
              </a>
              <hr />
            </Fragment>
          )}
          {other && (
            <Fragment>
              <Computer className={classes.span} color="primary" />
              {other}
              <hr />
            </Fragment>
          )}
          <CalendarToday className={classes.span} color="primary" />{" "}
          <span>Joined {dayjs(createdAt).format("MMM YYYY")}</span>
        </div>
      </div>
    </div>
  );
};

StaticProfile.propTypes = {
  profile: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(StaticProfile);
