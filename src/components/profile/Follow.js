import React, { Component } from "react";
import PropTypes from "prop-types";
import MyButton from "../../util/MyButton";

//MUI Stuff
import CheckCircle from "@material-ui/icons/CheckCircle";
import CheckCircleOutline from "@material-ui/icons/CheckCircleOutline";

// Redux Stuff
import { connect } from "react-redux";
import { followUser, unFollowUser } from "../../redux/actions/userActions";

class Follow extends Component {
  followedUser = () => {
    if (this.props.username && this.props.following.length > 0) return true;
    else return false;
  };

  handleFollow = () => {
    this.props.followUser(this.props.username);
  };

  handleUnFollow = () => {
    this.props.unFollowUser(this.props.username);
  };

  handleInFollowing = (username, followingArray) => {
    let isFoll = false;
    followingArray.forEach((el) => {
      if (el.recipient === username) {
        isFoll = true;
      }
    });
    return isFoll;
  };

  render() {
    const { authenticated, following } = this.props;

    let isFoll = this.handleInFollowing(this.props.username, following);
    const followButton =
      !authenticated ||
      this.props.username ===
        this.props.currentUser ? null : this.followedUser() && isFoll ? (
        <MyButton tip="Prestani pratiti" onClick={this.handleUnFollow}>
          <div>{"PRATIŠ"}</div>
          <CheckCircle
            color="primary"
            style={{
              marginLeft: 10,
            }}
          />
        </MyButton>
      ) : (
        <MyButton tip="Počni pratiti" onClick={this.handleFollow}>
          <div>{"PRATI"}</div>
          <CheckCircleOutline
            color="primary"
            style={{
              marginLeft: 10,
            }}
          />
        </MyButton>
      );
    return followButton;
  }
}

Follow.propTypes = {
  followUser: PropTypes.func.isRequired,
  unFollowUser: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
  following: PropTypes.array.isRequired,
  user: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated,
  following: state.user.following,
  currentUser: state.user.credentials.username,
});

const mapActionsToProps = { followUser, unFollowUser };

export default connect(mapStateToProps, mapActionsToProps)(Follow);
